#!/bin/bash
if [ $# -ne 1 ]; then
    echo "Usage: bash script.sh <c>"
    exit 1
fi

CARACTER=$1
PROPOZITII_CORECTE=0

function prima_litera_mare() {
    propozitie=$1
    prima_litera=${propozitie:0:1}
    if [[ $prima_litera == [A-Z] ]]; then
        return 0
    else
        return 1
    fi
}


function ultima_litera_corecta() {
    propozitie=$1
    lungime=${#propozitie}
    ultimul_caracter=${propozitie:lungime-1:1}
    if [[ $ultimul_caracter == "." || $ultimul_caracter == "!" || $ultimul_caracter == "?" ]]; then
        return 0
    else
        return 1
    fi
}

function caractere_valide() {
    propozitie=$1

    if [[ $propozitie =~ (, si) ]]; then
    return 1
    fi

    if [[ "$propozitie" =~ [A-za-z0-9,.\"!?\n\t]+$ ]]; then
        return 0
    else 
        return 1
    fi
}

function contine_caracter(){
    if [[ $propozitie =~ $CARACTER ]]; then
        return 0
    else
        return 1
    fi
}


while read -r propozitie; do
    if prima_litera_mare "$propozitie" && ultima_litera_corecta "$propozitie" && caractere_valide "$propozitie" && contine_caracter "$propozitie" ; then
        PROPOZITII_CORECTE=$((PROPOZITII_CORECTE + 1))
    fi
done

echo "Numarul de propozitii corecte: $PROPOZITII_CORECTE propozitii corecte care contin caracterul $CARACTER"