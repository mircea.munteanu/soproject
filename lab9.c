#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <libgen.h>
#include <time.h>

int counterProcese = 0;
char globalArgv4[1];

void no_argc(int argc)
{
    if (argc != 4)
    {
        printf("Trebuie sa fie trei arguemnte");
        exit(EXIT_FAILURE);
    }
}

DIR *openDir(char *argv)
{
    DIR *dir_pointer = opendir(argv);
    if (dir_pointer == NULL)
    {
        printf("Eroare la opendir!");
        exit(EXIT_FAILURE);
    }
    return dir_pointer;
}

void checkClosedir(DIR *dir_pointer)
{
    if (closedir(dir_pointer) == -1)
    {
        printf("Eroare la close la directoriu principal");
        exit(EXIT_FAILURE);
    }
}

void constructFilePath(char *filepath, struct dirent *entry, char *argv)
{
    strcpy(filepath, argv);
    strcat(filepath, "/");
    strcat(filepath, entry->d_name);
}

void checkClose(int fd)
{
    if (close(fd) == -1)
    {
        printf("Eroare la close!");
        exit(EXIT_FAILURE);
    }
}

void checkStat(char *filepath, struct stat *file_info)
{
    if (stat(filepath, file_info) == -1)
    {
        printf("Eroare la stat!");
        exit(EXIT_FAILURE);
    }
}

void checkLstat(char *filepath, struct stat *file_info)
{
    if (lstat(filepath, file_info) == -1)
    {
        printf("Eroare la lstat!");
        exit(EXIT_FAILURE);
    }
}

void checkLseek(int fd, int offset)
{
    if (lseek(fd, offset, SEEK_SET) == -1)
    {
        printf("Eroare la lseek!");
        exit(EXIT_FAILURE);
    }
}

void drepturi(struct stat *file_info, char *stringStatistica)
{
    char stringBuffer[1000] = "";

    strcat(stringBuffer, "Drepturile User-ului:\t");
    if (file_info->st_mode & S_IRUSR)
    {
        strcat(stringBuffer, "R");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    if (file_info->st_mode & S_IWUSR)
    {
        strcat(stringBuffer, "W");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    if (file_info->st_mode & S_IXUSR)
    {
        strcat(stringBuffer, "X");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    strcat(stringBuffer, "\n");

    strcat(stringBuffer, "Drepturi group:\t");
    if (file_info->st_mode & S_IRGRP)
    {
        strcat(stringBuffer, "R");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    if (file_info->st_mode & S_IWGRP)
    {
        strcat(stringBuffer, "W");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    if (file_info->st_mode & S_IXGRP)
    {
        strcat(stringBuffer, "X");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    strcat(stringBuffer, "\n");

    strcat(stringBuffer, "Drepturi other:\t");

    if (file_info->st_mode & S_IROTH)
    {
        strcat(stringBuffer, "R");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    if (file_info->st_mode & S_IWOTH)
    {
        strcat(stringBuffer, "W");
    }
    else
    {
        strcat(stringBuffer, "-");
    }
    if (file_info->st_mode & S_IXOTH)
    {
        strcat(stringBuffer, "X");
    }
    else
    {
        strcat(stringBuffer, "-");
    }

    strcat(stringBuffer, "\n");
    strcat(stringStatistica, stringBuffer);
}

void scriereInFisier(char *directory, char *filename, char *stringStatistica)
{
    char filePath[256];
    sprintf(filePath, "%s/%s", directory, filename);

    int fisierStatistica = open(filePath, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fisierStatistica == -1)
    {
        printf("Eroare la deschiderea fisierului de iesire!");
        exit(EXIT_FAILURE);
    }

    if (write(fisierStatistica, stringStatistica, strlen(stringStatistica)) == -1)
    {
        printf("Eroare la write!");
        exit(EXIT_FAILURE);
    }

    if (close(fisierStatistica) == -1)
    {
        printf("Eroare la inchiderea fisierului de iesire!");
        exit(EXIT_FAILURE);
    }
}

uint8_t grayify(uint8_t red, uint8_t green, uint8_t blue)
{
    return (uint8_t)(0.299 * red + 0.587 * green + 0.114 * blue);
}

void prelucrareBMP(struct stat *file_info, char *filepath, char *argv, char *stringStatistica)
{
    int pid = fork();
    if (pid < 0)
    {
        printf("Eroare cand cream pid pentru procesarea imaginii");
        exit(EXIT_FAILURE);
    }
    if (pid == 0)
    {
        exit(0);
    }
    counterProcese++;
    char stringBuffer[1000];
    int fd = open(filepath, O_RDWR);
    if (fd == -1)
    {
        printf("Eroare cand deschidem directorul!");
        exit(EXIT_FAILURE);
    }
    checkLseek(fd, 18);
    int latime;
    if (read(fd, &latime, sizeof(int)) == -1)
    {
        printf("Eroare cand deschidem read la latime!");
        exit(EXIT_FAILURE);
    }
    int inaltime;
    if (read(fd, &inaltime, sizeof(int)) == -1)
    {
        printf("Eroare cand deschidem read la inaltime!");
        exit(EXIT_FAILURE);
    }
    sprintf(stringBuffer, "Inaltimea este %d.\nLatimea este %d.\n\n", inaltime, latime);
    strcat(stringStatistica, stringBuffer);

    printf("Procesam imaginea cu dimensiunile: %d x %d\n", inaltime, latime);
    checkLseek(fd, 0);

    uint8_t *spatiuImg = (uint8_t *)malloc(inaltime * latime * 3);

    if ((spatiuImg) == NULL)
    {
        printf("Eroare la alocare");
        exit(EXIT_FAILURE);
    }
    checkLseek(fd, 54);
    if (read(fd, spatiuImg, inaltime * latime * 3) == -1)
    {
        printf("Eroare la read!");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < inaltime; i++)
    {
        for (int j = 0; j < latime; j++)
        {
            unsigned long pozitie = (i * latime + j) * 3;
            uint8_t *culoriRGB = &spatiuImg[pozitie];

            uint8_t blue = culoriRGB[0];
            uint8_t green = culoriRGB[1];
            uint8_t red = culoriRGB[2];

            uint8_t gri = grayify(red, green, blue);

            culoriRGB[0] = gri;
            culoriRGB[1] = gri;
            culoriRGB[2] = gri;
        }
    }
    checkLseek(fd, 54);
    if (write(fd, spatiuImg, inaltime * latime * 3) == -1)
    {
        printf("Eroare la write!");
        exit(EXIT_FAILURE);
    }
    free(spatiuImg);
    if (close(fd) == -1)
    {
        printf("Eroare cand dam close!");
        exit(EXIT_FAILURE);
    }
}

void counterLinii(char *stringStatistica)
{
    int c = 0;
    for (int i = 0; i < strlen(stringStatistica); i++)
    {
        if (stringStatistica[i] == '\n')
        {
            c++;
        }
    }
    exit(c);
}

void prelucrareFisierNormal(struct stat *file_info, char *filepath, char *argv)
{
    char stringStatistica[1000];
    sprintf(stringStatistica, "Nume fisier %s\nOcteti %ld.\nIdentificatorul user-ului este %d\nData ultimei modificari %s\nNumarul de legaturi %ld.\n\n", filepath, file_info->st_size, file_info->st_uid, ctime(&file_info->st_mtime), file_info->st_nlink);

    char filename[100];
    sprintf(filename, "%s_statistica.txt", basename(filepath));

    if (strstr(filename, ".bmp"))
    {
        prelucrareBMP(file_info, filepath, argv, stringStatistica);
        drepturi(file_info, stringStatistica);
        scriereInFisier(argv, filename, stringStatistica);
        counterLinii(stringStatistica);
        return;
    }

    drepturi(file_info, stringStatistica);
    scriereInFisier(argv, filename, stringStatistica);
    char dirwithfilename[1000];
    sprintf(dirwithfilename, "%s/%s", argv, filename);

    int pfd1[2];
    int pfd2[2];
    int cat_pid, bash_pid;
    int status_cat, status_wc;

    if (pipe(pfd1) == -1)
    {
        perror("Eroare la crearea pipe 1");
        exit(EXIT_FAILURE);
    }

    if (pipe(pfd2) == -1)
    {
        perror("Eroarea la crearea pipe 2");
        exit(EXIT_FAILURE);
    }

    if ((cat_pid = fork()) == -1)
    {
        perror("Error forking cat child");
        exit(EXIT_FAILURE);
    }

    if (cat_pid == 0)
    {
        checkClose(pfd1[0]);
        checkClose(pfd2[0]);
        checkClose(pfd2[1]);

        if (dup2(pfd1[1], STDOUT_FILENO) == -1)
        {
            perror("Eroare in dup2 pentru cat");
            exit(EXIT_FAILURE);
        }
        checkClose(pfd1[1]);

        execlp("cat", "cat", dirwithfilename, NULL);
        perror("Eroare in execlp pentru cat");
        exit(EXIT_FAILURE);
    }
    else
    {
        checkClose(pfd1[1]);

        if ((bash_pid = fork()) == -1)
        {
            perror("Eroare fork bash_pid");
            exit(EXIT_FAILURE);
        }
        if (bash_pid == 0)
        {

            checkClose(pfd2[0]);

            if (dup2(pfd1[0], STDIN_FILENO) == -1)
            {
                printf("Eroare la script la STDIN_FILENO");
                exit(EXIT_FAILURE);
            }
            checkClose(pfd1[0]);
            if (dup2(pfd2[1], STDOUT_FILENO) == -1)
            {
                printf("Eroare la script la STDOUT_FILENO");
                exit(EXIT_FAILURE);
            }
            checkClose(pfd2[1]);

            execlp("bash", "bash", "script.sh", globalArgv4, NULL);
            perror("Eroare in execlp pentru script");
            exit(EXIT_FAILURE);
        }
        else
        {
            checkClose(pfd2[1]);

            char buffer[1024] = {0};
            unsigned long bytesRead;

            while ((bytesRead = read(pfd2[0], &buffer, sizeof(buffer))) > 0)
                ;
            if (bytesRead == -1)
            {
                printf("Eroare la citirea octetilor");
                exit(EXIT_FAILURE);
            }
            printf("%s", buffer);

            checkClose(pfd2[0]);

            checkClose(pfd1[0]);

            if (waitpid(cat_pid, &status_cat, 0) == -1)
            {
                perror("Eroare la wait la cat_pid");
                exit(EXIT_FAILURE);
            }

            if (waitpid(bash_pid, &status_wc, 0) == -1)
            {
                perror("Eroare la wait la bash_pid");
                exit(EXIT_FAILURE);
            }
        }
    }
    counterLinii(stringStatistica);
}

void prelucrareLegatura(struct stat *file_info, char *filepath, char *argv)
{
    char stringStatistica[1000];
    sprintf(stringStatistica, "Nume legatura %s.\nDimensiune legatura %ld?\nDimensiunea fisierului target %ld\n\n", filepath, file_info->st_size, file_info->st_blksize);

    char filename[100];
    sprintf(filename, "%s_statistica.txt", basename(filepath));

    drepturi(file_info, stringStatistica);
    scriereInFisier(argv, filename, stringStatistica);
    counterLinii(stringStatistica);
}

void prelucrareDirector(struct stat *file_info, char *filepath, char *argv)
{
    char stringStatistica[1000];
    sprintf(stringStatistica, "Nume director: %s\nIdentificatorul utilizatorului : %d\n\n", filepath, file_info->st_uid);

    char filename[1000];
    sprintf(filename, "%s_statistica.txt", basename(filepath));

    drepturi(file_info, stringStatistica);
    scriereInFisier(argv, filename, stringStatistica);
    counterLinii(stringStatistica);
}

void extragereStatusFisiere()
{
    for (int i = 0; i < counterProcese; i++)
    {
        int status;
        int pid;
        if ((pid = wait(&status)) == -1)
        {
            printf("Eroare la procesul %d", i);
            exit(EXIT_FAILURE);
        }
        int fd = open("statistica.txt", O_WRONLY | O_APPEND | O_CREAT);
        if (fd == -1)
        {
            printf("Nu s-a putut deschide statistica.txt");
            exit(EXIT_FAILURE);
        }
        if (WIFEXITED(status))
        {
            int statusIesire = WEXITSTATUS(status);

            char stringCounter[200];
            sprintf(stringCounter, "Numarul de linii este:%d\n", statusIesire);
            if (write(fd, stringCounter, strlen(stringCounter)) == -1)
            {
                printf("Eroare la write!");
                exit(EXIT_FAILURE);
            }

            printf("PID fiu:%d si status este %d\n", pid, statusIesire);
        }
        checkClose(fd);
    }
}

void checkArgv3(char argv3[])
{
    if (strlen(argv3) != 1)
    {
        printf("argv3 tre sa fie un caracter!");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv)
{
    no_argc(argc);
    checkArgv3(argv[3]);
    strcpy(globalArgv4, argv[3]);

    DIR *dir_pointer_intrare = openDir(argv[1]);
    DIR *dir_pointer_iesire = openDir(argv[2]);

    struct dirent *entry;
    while ((entry = readdir(dir_pointer_intrare)) != NULL)
    {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
        {
            continue;
        }
        int pid;
        if ((pid = fork()) < 0)
        {
            perror("Eroare la pid = fork()");
            exit(EXIT_FAILURE);
        }

        if (pid == 0)
        {
            char filepath[100];
            constructFilePath(filepath, entry, argv[1]);

            struct stat file_info;
            struct stat file_info2;

            checkStat(filepath, &file_info);
            checkLstat(filepath, &file_info2);

            if (S_ISREG(file_info.st_mode))
            {
                prelucrareFisierNormal(&file_info, filepath, argv[2]);
            }
            if (S_ISLNK(file_info2.st_mode))
            {
                prelucrareLegatura(&file_info2, filepath, argv[2]);
            }
            if (S_ISDIR(file_info2.st_mode))
            {
                prelucrareDirector(&file_info2, filepath, argv[2]);
            }
        }
        else
        {
            counterProcese++;
        }
    }
    extragereStatusFisiere();
    checkClosedir(dir_pointer_intrare);
    checkClosedir(dir_pointer_iesire);

    return 0;
}