This project is a C program designed to process and analyze files within a specified directory. Here's a brief description of its functionality:

Command-Line Arguments: The program requires three command-line arguments:

Source directory (containing files to process),
Output directory (where statistical files are written),
A single character argument.
File Processing:

It opens the specified source directory, reads its contents, and iterates over each file.
For regular files, it generates statistics such as file size, user ID, modification time, number of hard links, and permissions. If the file is a BMP image, it performs additional processing to convert it to grayscale.
For symbolic links, it generates specific statistics about the link and target file.
For directories, it generates basic information, like the user ID.
Parallel Processing:

The program uses fork() to create child processes for processing individual files in parallel.
It also spawns child processes for executing external commands like cat and a Bash script.
Output: The statistics for each processed file are written to separate output files in the specified output directory. For BMP images, the grayscale-converted version is written back into the original file.

Final Output: After processing all files, the program waits for all child processes to finish and records the number of lines processed in each file in a final summary file (statistica.txt).